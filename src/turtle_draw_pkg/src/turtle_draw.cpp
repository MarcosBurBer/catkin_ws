#include "ros/ros.h"
#include "std_msgs/String.h"
#include "geometry_msgs/Twist.h"

#include <math.h>

int main(int argc, char **argv)
{
  ros::init(argc, argv, "drawer");

  ros::NodeHandle node;

  ros::Rate loop_rate(1);

  ros::Publisher turtlesim_pub = node.advertise<geometry_msgs::Twist>("/turtle1/cmd_vel", 100);
  while (turtlesim_pub.getNumSubscribers() == 0) {
     loop_rate.sleep();
  }
  geometry_msgs::Twist moveForwardMsg;
  moveForwardMsg.linear.x = 2.0;
  moveForwardMsg.angular.z = 0;
  geometry_msgs::Twist rotateMsg;
  rotateMsg.angular.z = (2*M_PI) / 4;

  for (int i=0; i<4; i++) {
    turtlesim_pub.publish(rotateMsg);
    loop_rate.sleep();
    turtlesim_pub.publish(moveForwardMsg);
    loop_rate.sleep();
  }

  return 0;
}
